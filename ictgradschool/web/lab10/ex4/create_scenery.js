/**
 * Created by qpen546 on 6/04/2017.
 */
"use strict";
$("input[type=radio]").each(function (i, obj) {
    $(this).click(function () {
        if (i + 1 == 6) {
            $("#background").attr("src", "../images/ex4/background" + (i + 1) + ".gif");
        } else {
            $("#background").attr("src", "../images/ex4/background" + (i + 1) + ".jpg");
        }
    });
});

$("input[type=checkbox]").each(function (i, checkbox) {

    var dolphinId = $(checkbox).attr("id");

    dolphinId = "#" + dolphinId.substring((dolphinId.indexOf('-') + 1));

    if (checkbox.checked) {
        $(dolphinId).css("visibility", "visible");
    } else {
        $(dolphinId).css("visibility", "hidden");
    }

    $(checkbox).change(function () {

        if (checkbox.checked) {
            $(dolphinId).css("visibility", "visible");
        } else {
            $(dolphinId).css("visibility", "hidden");
        }

    });
});

var originalPostion = new Array;
var backgroundWidth = $("#background").width();
var backgroundHeight = $("#background").height();
$("label[for=vertical-control]").text("vertical control: 0");
$("label[for=size-control]").text("size: 0");
$("label[for=horizontal-control]").text("horizontal shift: 0");


var originalWidth = new Array;
$(".dolphin").each(function (i, obj) {
    originalWidth[i] = $(this).width();
    originalPostion[i] = $(this).position();
})


$("#vertical-control").slider({
    value: 0,
    min: -50,
    max: 50,
    step: 1,
    orientation: "vertical",
    slide: function (event, ui) {
        //Resize image
        $(".dolphin").each(function (i, obj) {
            var fraction = -backgroundHeight * ui.value / 100;
            var newTop = originalPostion[i].top + fraction;
            $(this).css("top", newTop);
            $("label[for=vertical-control]").text("vertical control: " + fraction.toFixed(2));
        })
    }
});

$("#size-control").slider({
    value: 0,
    min: -50,
    max: 50,
    step: 1,
    slide: function (event, ui) {
        //Resize image
        $(".dolphin").each(function (i, obj) {
            var fraction = 1 + ui.value / 100;
            var newWidth = originalWidth[i] * fraction;
            $(this).width(newWidth);
            $("label[for=size-control]").text("size: " + fraction.toFixed(2));
        })
    }
});

$("#horizontal-control").slider({
    value: 0,
    min: -50,
    max: 50,
    step: 1,
    slide: function (event, ui) {
        //Resize image
        $(".dolphin").each(function (i, obj) {
            var fraction = backgroundWidth * ui.value / 100;
            var newLeft = originalPostion[i].left + fraction;
            $(this).css("left", newLeft);
            $("label[for=horizontal-control]").text("horizontal shift: " + fraction.toFixed(2));
        })
    }
});
